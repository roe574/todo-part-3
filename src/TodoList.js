import React, { Component } from 'react';
import TodoItem from './TodoItem.js';
import { connect } from 'react-redux';
import { toggleTodo, deleteTodo } from './actions.js'
class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map(todo => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              handleToggleComplete={event => { this.props.toggleTodo(todo.id) }}
              handleToggleDelete={event => { this.props.deleteTodo(todo.id) }}
            />
          ))}
        </ul>
      </section>
    );
  };
};

const mapDispatchToProps = {
  toggleTodo,
  deleteTodo
}
export default connect(null, mapDispatchToProps)(TodoList);