import todosList from './todos.json';
import {
    ADD_TODO,
    TOGGLE_TODO,
    DELETE_TODO,
    CLEAR_COMPLETED_TODOS
} from './actions.js'

const initialState = {
    todos: todosList
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TODO: {
            const newTodoList = state.todos.slice();
            newTodoList.push(action.payload)
            return { todos: newTodoList }
        };
        case TOGGLE_TODO: {
            const newTodos = state.todos.slice();
            const newnewTodos = newTodos.map(todo => {
                if (todo.id === action.payload) {
                    todo.completed = !todo.completed
                }
                return todo;
            });
            return { todos: newnewTodos };
        };
        case DELETE_TODO: {
            const deleteTodos = state.todos.filter(todo =>
                todo.id !== action.payload);
            return { todos: deleteTodos };
        };
        case CLEAR_COMPLETED_TODOS: {
            const clearCompletedTodos = state.todos.filter(todo => !todo.completed);
            return { todos: clearCompletedTodos };

        }
        default:
            return state
    }
}

export default reducer