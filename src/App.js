import React, { Component } from 'react';
import TodoList from './TodoList.js'
import './index.css';
import todosList from './todos.json';
import { Route, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { addTodo, clearCompletedTodos } from './actions.js'


class App extends Component {
  state = {
    todos: todosList
  };

  handleToggleClearCompleted = () => {
    this.props.clearCompletedTodos();
  };

  handleAddToDo = event => {
    if (event.key === "Enter") {
      this.props.addTodo(event.target.value)
      event.target.value = "";
    };
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>Chore List</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            onKeyDown={this.handleAddToDo}
            autofocus
          />
        </header>
        <Route
          exact
          path="/"
          render={() => (
            <TodoList
              todos={this.props.todos} />)}
        />

        <Route path="/active"
          render={() => (
            <TodoList
              todos={this.props.todos.filter(todo =>
                todo.completed === false)} />)}
        />

        <Route path="/completed"
          render={() => (
            <TodoList
              todos={this.props.todos.filter(todo =>
                todo.completed === true)} />)}
        />

        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
            <strong>{this.props.todos.filter(todo => !todo.completed).length}</strong> item(s) left
  </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">
                All
              </NavLink>
            </li>
            <li>
              <NavLink to="/active" activeClassName="selected">
                Active
              </NavLink>
            </li>
            <li>
              <NavLink to="/completed" activeClassName="selected" >
                Completed
              </NavLink>
            </li>
          </ul>
          <button className="clear-completed"
            type="clear"
            checked={this.props.completed}
            onClick={this.handleToggleClearCompleted}>Clear completed</button>
        </footer>
      </section>

    );
  };
};

const mapStateToProps = state => {
  return {
    todos: state.todos
  };
};

const mapDispatchToProps = {
  addTodo,
  clearCompletedTodos,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
